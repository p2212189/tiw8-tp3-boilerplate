import { useEffect, useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useAppSelector } from '../hooks'
import { setValueStream } from '../slices/board-slice'
import { AppDispatch, peer } from '../store'

export const VideoComponent: React.FC = () => {
  const [startAvailable, setStart] = useState(true)
  const [callAvailable, setCall] = useState(false)
  const [hangupAvailable, setHangup] = useState(false)

  interface Point {
    x: number,
    y: number
  }

  const dispatch = useDispatch<AppDispatch>()

  let remoteVideo = useAppSelector((state) => state.remoteStream) as MediaStream
  let localVideo = useAppSelector((state) => state.localStream) as MediaStream

  let playerPosition = useAppSelector((state) => state.playerPosition)
  let remotePlayerPostion = useAppSelector((state) => state.remotePlayerPosition)

  let localVideoRef = useRef<HTMLVideoElement>(null)
  let remoteVideoRef = useRef<HTMLVideoElement>(null)

  let isLoggedIn = false;
  let isStreamed = false;

  const call = () => {
    setCall(true)
    setHangup(true)
    setCall(false)
    if (remoteVideo !== null) {
      gotRemoteStream(remoteVideo)
    }
  }

  const hangup = () => {
    setCall(true)
    setHangup(true)
    isLoggedIn = false
  }

  const gotRemoteStream = (remoteStream: MediaStream) => {
    if (remoteStream !== null) {
      if (remoteVideoRef.current !== null && remoteStream.getTracks !== undefined) {
        remoteVideoRef.current.srcObject = remoteStream;
      }
    }
  }

  const start = () => {
    navigator.mediaDevices
      .getUserMedia({
        audio: true,
        video: true
      })
      .then(stream => {
        if (stream !== null) {
          gotStream(stream)
        }
      })
      .catch((e) => {
        console.log(e)
        alert('getUserMedia() error:' + e.name)
      })
    setCall(true)
    setStart(false)
  }

  const gotStream = (stream: MediaStream) => {
    console.log('this is my stream', stream)
    if (localVideoRef.current !== null) {
      localVideoRef.current.srcObject = stream // on injecte le flux vidéo local dans l'element video qui a pour ref 'localVideoRef'
      console.log('this is local video Ref', localVideoRef)
    }
    if (stream !== null) {
      peer.addStream(stream)
      isStreamed = true;
      dispatch(setValueStream([stream, 'local'], true)) // sera utile plus tard pour avoir accès au flux dans le middleware
    }
  }

  function calculateDistance(p1: Point, p2: Point): number {
    const xDiff = p2.x - p1.x;
    const yDiff = p2.y - p1.y;
    return Math.sqrt(xDiff ** 2 + yDiff ** 2);
  }


  useEffect(() => {
    const distance = calculateDistance({ x: playerPosition[0], y: playerPosition[1] }, { x: remotePlayerPostion[0], y: remotePlayerPostion[1] });
    if (distance < 7 && distance > 0) {
      if (isStreamed === false) {
        start()
        isStreamed = true
      }
      call();
      isLoggedIn = true;
    } else if (distance === 1) {
      console.log("continue")
    } else {
      hangup();
      isLoggedIn = false;
    }
  }, [playerPosition, remotePlayerPostion]);

  return (
    <div>
      <div>
        <div>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
            onClick={start} disabled={!startAvailable}>
            Start
                </button>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
            onClick={call} disabled={!callAvailable}>
            Call
                </button>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
            onClick={hangup} disabled={!hangupAvailable}>
            HangUp
                </button>
        </div>

        {!isLoggedIn &&
          <video
            id="video1"
            ref={localVideoRef}
            autoPlay
            muted={false}
          >
            <track kind="captions" srcLang="en" label="english_captions" />
          </video>
        }
        {!isLoggedIn &&
          <video
            id="video2"
            ref={remoteVideoRef}
            autoPlay
          >
            <track kind="captions" srcLang="en" label="english_captions" />
          </video>
        }
      </div>
    </div>
  )
}
