import { FC } from 'react'
import { Board } from './components/board'
/*eslint-disable */
const App: FC = () => {
  return (
    <div>
      <Board />
    </div>
  )
};

export default App;
