import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import SimplePeer, { Instance } from 'simple-peer'

// Define a type for the slice state
interface AppState {
  peers: Map<string, Instance>
  playerPosition: [number, number]
  playerAvatar: string
  remotePlayerPosition: [number, number]
  remotePlayerAvatar: string
  remotePlayerId: string
  localStream: MediaStream | null;
  remoteStream: MediaStream | null;
  peer: SimplePeer.Instance;
  board: {
    width: number
    height: number
    tiles: Tile[]
  }
}

interface peerData {
  peerId: string; peer?: Instance
}

interface remotePlayer {
  stream?: MediaStream
  currentlyStreamingTo?: boolean
}

interface peerDataInfo {
  peerId: string; peer?: Instance
}

interface State {
  localVideoRef: HTMLVideoElement | null;
  remoteVideoRef: HTMLVideoElement | null;
}

// Define the initial state using that type
const initialState: AppState = {
  playerPosition: [10, 24],
  playerAvatar: '',
  remotePlayerPosition: [10, 25],
  remotePlayerAvatar: '',
  remotePlayerId: '',
  localStream: null,
  remoteStream: null,
  peer: new SimplePeer(),
  board: {
    width: 60,
    height: 60,
    tiles: [] // unused for now, could be useful for collision management
  },
  peers: new Map<string, Instance>()

}

export const boardSlice = createSlice({
  name: 'board',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    movePlayer: {
      reducer (state, action: PayloadAction<[[number, number], string]>) {
        console.log(action)
        if (action.payload[1] === 'local') {
          state.playerPosition = action.payload[0]
        }
        if (action.payload[1] === 'remote') {
          state.remotePlayerPosition = action.payload[0]
        }
      },
      prepare (payload: [[number, number], string], propagate: boolean) {
        return { payload, meta: { propagate } }
      }
    },
    setValueStream: {
      reducer (state, action: PayloadAction<[MediaStream | null, string]>) {
        console.log(action)
        if (action.payload[1] === 'local') {
          state.localStream = action.payload[0]
        }
        if (action.payload[1] === 'remote') {
          state.remoteStream = action.payload[0]
        }
      },
      prepare (payload: [MediaStream | null, string], propagate: boolean) {
        return { payload, meta: { propagate } }
      }
    },
    setPeer: {
      reducer (state, action: PayloadAction<{peer: SimplePeer.Instance}>) {
        state.peer = action.payload.peer
      },
      prepare (payload: {peer: Instance}, propagate: boolean) {
        return { payload, meta: { propagate } }
      }
    },
    setAvatar: {
      reducer (state, action: PayloadAction<[string, string]>) {
        console.log(action)
        if (action.payload[1] === 'local') {
          state.playerAvatar = action.payload[0]
        }
        if (action.payload[1] === 'remote') {
          state.remotePlayerAvatar = action.payload[0]
        }
      },
      prepare (payload: [string, string], propagate: boolean) {
        return { payload, meta: { propagate } }
      }
    }
  }
})

export const { movePlayer, setAvatar, setPeer, setValueStream } = boardSlice.actions

// Other code such as selectors can use the imported `RootState` type
// export const selectCount = (state: RootState) => state.slidesApp.value

export default boardSlice.reducer
