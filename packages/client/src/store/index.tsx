import { configureStore } from '@reduxjs/toolkit'
import { logger } from 'redux-logger'
import boardReducer, { setPeer, setValueStream } from '../slices/board-slice'
import { io } from 'socket.io-client'
import { Middleware, AnyAction, Dispatch } from 'redux'
import SimplePeer, { Instance } from 'simple-peer'

const socket = io()
const useTrickle = true
export let peer: Instance

const WebSocketMiddleware: Middleware<Dispatch> =
  (api) => (next) => {
    return (action: AnyAction) => {
      if (peer === null) {
        return
      }
      if (action.meta.propagate) {
        peer.send(JSON.stringify(action))
      }
      return next(action)
    }
  }
export const store = configureStore({
  reducer: boardReducer,
  middleware: [logger, WebSocketMiddleware]
})

const addPeer = (socketId: string, initiator: boolean) => {
  const config = {
    iceServers: [
      {
        urls: ['stun:codinget.me:8080', 'stun:codinget.me:3128']
      },
      {
        urls: [
          'turn:codinget.me:8080',
          'turn:codinget.me:3128',
          'turn:codinget.me:8080?transport=tcp',
          'turn:codinget.me:3128?transport=tcp'
        ],
        username: 'webrtc',
        credential: 'webrtc'
      }
    ]
  }

  peer = new SimplePeer({
    initiator,
    trickle: useTrickle, // useTrickle doit être a true pour que le peer persiste
    config
  })

  if (peer) {
    peer.on('signal', function (signal) {
      console.log('this is on signal ', signal)
      socket.emit('signal', { signal, socketId })
    })

    peer.on('connect', function () {
      console.log('Peer connection established .....')
      console.log('PEEEEER  WORKING')
      console.log('Im on connect peer', peer)
      setPeer({ peer }, true)
    })

    peer.on('error', function (err) {
      console.log('Error Sending data to peer', err)
    })

    peer.on('data', function (data) {
      const action = JSON.parse(data)
      action.meta.propagate = false
      if (action.type === 'board/setAvatar' || action.type === 'board/movePlayer' || action.type === 'board/setValueStream') {
        action.payload[1] = 'remote'
        store.dispatch(action)
      }
    })

    peer.on('stream', stream => {
      store.dispatch(setValueStream([stream, 'remote'], false))
    })

    socket.on('signal', function (data) {
      const { signal } = data
      if (peer) {
        peer.signal(signal)
      }
    })
  }
}

socket.on('connect', function () {
  console.log('connection ====>')
})

socket.on('peer', function (data) {
  addPeer(data.socketId, data.initiator)
})

socket.on('removePeer', function (socketId) {
  console.log('REMOVE PEER', socketId)
})

socket.on('disconnect', function () {
  console.log('disconnected')
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
