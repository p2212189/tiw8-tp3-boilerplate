import express from 'express'
import { Server } from 'socket.io'
import http from 'http'
import path from 'path'

const app = express()
const port = process.env.PORT || 3000

const server = http.createServer(app)
const io = new Server(server)

const DIST_DIR = path.join(__dirname, '../../client/dist')
const HTML_FILE = path.join(DIST_DIR, 'index.html')

const mockResponse = {
  foo: 'bar',
  bar: 'foo'
}
app.get('/api', (req, res) => {
  res.send(mockResponse)
})

server.listen(port, function () {
  console.log('App listening on port: ' + port)
})

app.use(express.static(DIST_DIR))

app.get('/*', (req, res) => {
  res.sendFile(HTML_FILE)
})

let initiator = true

io.on('connection', function (socket) {
  console.log('a user connected to the server', socket.id)
  initiator = !initiator

  socket.emit('peer', { socketId: socket.id, initiator })

  socket.on('signal', (data) => {
    const { socketId, signal } = data
    console.log('registration with >>>>>>>>>>', data)

    socket.broadcast.emit('signal', {
      socketId: socket.id,
      signal
    })
  })

  socket.on('disconnect', () => {
    socket.broadcast.emit('removePeer', socket.id)
    console.log('DISCONNECTED ', socket.id)
  })
})
