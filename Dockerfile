FROM node:19
ADD . /app
WORKDIR /app
RUN yarn install
RUN (cd packages/server && yarn install)
RUN (cd packages/client && yarn install && yarn build)
CMD (cd packages/server && yarn dev)
