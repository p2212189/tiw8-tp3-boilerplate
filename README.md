# tiw8-tp3-boilerplate

Sujet : https://aurelient.github.io/tiw8/2022/TP3/

Boilerplate inpired from : https://dev.to/alexeagleson/how-to-create-a-node-and-react-monorepo-with-git-submodules-2g83

L'objectif est de réaliser un clone simplifié de [Gathertown](https://www.gather.town/).

Ce code est la base du projet avec une map, et un perso qui bouge. 

## TIW8 - TP3 Collaboration temps-réel

## Team Members 

- El Biyaali Chaimae p2211263
- Larhchim Ismail p2212189

## Actual state of app

- Everything is running well except from hangup when the two avatars are farawy or when we click on Hangup button


## What we did
- we have to launch two tabs in order to connect two peers (if not there will be errors we must wait until we see the map inside the two tabs) after that we can add the two avatars each one inside the tab refering him and start moving them when the euclidien distance is less than 7 we launch the local camera and the remote tab camera before that a peer to peer connection is established successfully and we have the two peers connected to each other throgh signaling.

## Dependecies

- "lerna": "^6.5.1",
- "@babel/eslint-parser": "^7.19.1",
- "simple-peer": "^9.11.1",
- "socket.io-client": "^4.6.1",
- "tailwindcss": "^3.2.7"
- "typescript": "^4.9.5",
- "@types/react": "^18.0.27",
- "@types/react-dom": "^18.0.10",
- "@types/redux-logger": "^3.0.9",
- "@types/simple-peer": "^9.11.5",
- "@vitejs/plugin-react-swc": "^3.0.0",
- "react-error-overlay": "6.0.9",
- "typescript": "^4.9.5",
- "vite": "^4.1.0"
- "express": "^4.18.2",
- "socket.io": "^4.6.1"
- "@types/express": "^4.17.17",


## Setup 

- nvm install --lts
- nvm use 19
- yarn install
- cd packages/client && yarn install
- cd packages/server && yarn install
- yarn run lint to run linter in project root


## Build process Dev env

- cd packages/server ==> accessing the server folder
- yarn install ==> installing the dependencies for server
- cd packages/client ==> accessing the client folder
- yarn install ==> installing the dependencies for client
- cd .. ==> back to project root
- yarn dev ==> runing client as dev environement so the static files and js files are stored in the /dist folder in client side and also accessing server side and runing with node command the express server letting him know that we have the front end static files in the client side of ../../client/dist folder lerna parallel command is executing the dev command for client and server


## Build process Prod env

- FROM node:19
- ADD . /app
- WORKDIR /app
- RUN yarn install
- RUN (cd packages/server && yarn install)
- RUN (cd packages/client && yarn install && yarn build)
- CMD (cd packages/server && yarn dev)


## process of Deploy

Deploying the docker image in portainer env as ubunutu:20.04 image with the script as follow

deploy:
  image: ubuntu:20.04
  script:
    - apt update && apt install -y curl
    - curl -X POST --http1.1 https://portainer.uni.codinget.me/api/stacks/webhooks/5e81ade4-f5d3-410e-a51c-04f35897895e


## accessing the app live demo

https://tiw8-larhchim-webrtc.uni.codinget.me/
